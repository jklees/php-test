<?php

$offset =  $_POST['pokeOffset'];
grabPokeList($offset);

function grabPokeList($offset)
{
    $pokeUrl = "https://pokeapi.co/api/v2/pokemon?&offset=".$offset;

    $curl = curl_init();

    curl_setopt($curl, CURLOPT_URL, $pokeUrl);
    //curl_setopt($curl, CURLOPT_HEADER, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);


    $res = curl_exec($curl);
    

    $listDecode = json_decode($res);

    //var_dump($listDecode->results);
    $normalArray =[];
    
    foreach($listDecode->results as $result)
        {
            $tempArray=[];
            $tempArray['name'] = $result->name;
            $tempArray['url'] = $result->url;
            array_push($normalArray,$tempArray);

        }
    //   var_dump($normalArray);

    echo json_encode($normalArray);
}

?>