<?php
//check cache for pokemon
//if cache lacks pokemon
//grab from api
//store that pokemon
//load that pokemon in.

//user types something in search bar
//search bar ajax's to this script.
//this script checks cache 
//else queries pokemon api


if($_POST['search'] == NULL)
    {
        $err = array('code' =>'404','msg'=>'search not found');
        echo json_encode($err);
        exit;
    }



//queryApi("ditto");
//queryApi('doggo');
//queryCache('ditto');
queryCache($_POST['search']);
//queryApi('charmander');

function queryCache($search)
{
    //echo "\r\ncache QUERY\r\n";
    $safeSearch = urlencode($search);
    if(!file_exists(getcwd()."/cache/pokemon/".$safeSearch.".json"))
    {
        queryApi($search);
        exit;
    }

   $pokeFile = fopen(getcwd()."/cache/pokemon/".$safeSearch.".json",'r');
  
   $pokeRead = fread($pokeFile,filesize(getcwd()."/cache/pokemon/".$safeSearch.".json"));
   //var_dump(json_decode($pokeRead));
       fclose($pokeFile);

       $resDecode = json_decode($pokeRead);

       //load in stats and images.
     
       //  var_dump($resDecode);
  //store images and stats into an array????
       $poke = array('name' => $safeSearch, 'image' =>'/src/cache/images/'.$safeSearch.'.png', 'height' => $resDecode->height, 'weight' =>$resDecode->weight,'species' => $resDecode->species, 'abilities'=>$resDecode->abilities);

       //echo "\r\n\r\n";
   //var_dump($poke);
   echo json_encode($poke);

}

function queryApi($search)
{
    //url sanitise search
    //put it in a curl
    //send it off to pokeapi.co

    //echo "\r\nAPI QUERY\r\n";

    $pokeUrl = "https://pokeapi.co/api/v2/pokemon/";
    
    $safeSearch = urlencode($search);
    $pokeUrl .= $safeSearch."/";
    $curl = curl_init();

    curl_setopt($curl, CURLOPT_URL, $pokeUrl);
    //curl_setopt($curl, CURLOPT_HEADER, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($curl);
    curl_close($curl);
    //  echo "\r\n";
    if(@curl_getinfo($curl,CURLINFO_HTTP_CODE) == '404')
        {
            $err = array( 'code' =>'404', 'msg'=>'search not found');
             echo json_encode($err);
        }
    else
        {
            //      echo $response."\r\n\r\n";
            saveThatPokemon($response);
            //load in images and stats. 
        }
    
}


function saveThatPokemon($res)
{
    @mkdir(getcwd()."/cache");
    @mkdir(getcwd()."/cache/pokemon");

    $resDecode = json_decode($res);
    $pokeName = $resDecode->name;
    //var_dump($resDecode);

   $pokeFile =  fopen(getcwd()."/cache/pokemon/".$pokeName.".json",'w');
   fwrite($pokeFile,$res);

   fclose($pokeFile);
    
   //next step is to save the images 

   @mkdir(getcwd()."/cache/images");

   //var_dump($resDecode->sprites->back_default);
   //['back_default']
   //$pokeUrl = $resDecode->sprites->back_default;
   $pokeUrl = $resDecode->sprites->front_default;
   
   $curl = curl_init();

    curl_setopt($curl, CURLOPT_URL, $pokeUrl);
    //curl_setopt($curl, CURLOPT_HEADER, true);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($curl);
    curl_close($curl);


   $pokeFile =  fopen(getcwd()."/cache/images/".$pokeName.".png",'w');
   
   file_put_contents(getcwd()."/cache/images/log",$pokeFile,FILE_APPEND);

   @fwrite($pokeFile,$response);

   fclose($pokeFile);
    
   //store images and stats into an array????
   $poke = array('name' => $pokeName, 'image' =>'/src/cache/images/'.$pokeName.'.png', 'height' => $resDecode->height, 'weight' =>$resDecode->weight,'species' => $resDecode->species,'abilities'=>$resDecode->abilities);
   //var_dump($poke);
   //echo "\r\n\r\n";
   //var_dump($poke);
   echo json_encode($poke);
}


?>
