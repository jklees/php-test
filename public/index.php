<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Pokédex</title>
        <link rel="stylesheet" type="text/css" href="css/index.css">
    </head>
    <body>
        <script src="js/jquery-3.3.1.min.js"></script> 
      <div id="container">
      <div id="pokemonSearch">Seach Pokemon:<input id="search" type="text"> <button value="search" onclick="loadPokemon()">Need a little time</button></div>
          <div id="pokemonImage">
                <img id="poke" src="../src/cache/images/diglett.png" style="margin-left: 33%;margin-top: 14%;" >
      <div class="heightDiv">Height: <span id="pokemonHeight">poke</span></div>
                <div class="weightDiv">Weight: <span id="pokemonWeight">mon</span></div>
          </div>
      <div class="nameDiv">Name: <span id="pokemonName">Is</span></div>
      <div class="speciesDiv">Species: <span id='pokemonSpecies'>a</span></div>
      <div class="abilitiesDiv">Abilities: <span id='pokemonAbilities'>test</span></div>
       <br/>
      <div id="pokemonList">
      </div>
      <div id="buttons">
        <span onclick='nextList(0);' style='background-colour:grey;'> back </span><span onclick='nextList(1);' style='background-color:yellow;'>next</span>
      </div>
      </div>
   </body>
<script type="text/javascript">

      console.log($('#pokemonHeight'));

var pokeOffset = 0;
      
$(document).ready(function(){
loadPokemonList(pokeOffset);



});

function nextList(mode)
{  //mode 0 - move back 
    //mode 1 - move forward
    if(mode == 0)
    {
        if(pokeOffset == 0)
        {
            return; 
        }

        pokeOffset -= 20;        
        loadPokemonList(pokeOffset);
    }
    else if(mode == 1)
    {
        pokeOffset += 20;
        loadPokemonList(pokeOffset);

    }

}

function loadPokemon(name)
{
    console.log("Load POKEMON");

    console.log(name);
    if(typeof(name) === 'undefined')
    {  
        var pokemonName = $('#search').val();
        console.log("pokesearch Value is:" + $('#search').val());
        console.log("true");
    }
    else
    {
        console.log("false");
        var pokemonName = name;
        
    }
    console.log(pokemonName);
     ajaxCall(0,pokemonName);
}


function loadPokemonList(offset)
{
  ajaxCall(1,offset);

}


function ajaxCall(mode,varible)
{
    //mode 0 - api query
    //mode 1 - pokemonlist
    var url="";

    if(mode == 0)
    {
        url = "../src/queryApi.php";
        var obj= {'search': varible};

    }
    else if(mode == 1)
    {
        console.log("mode one");
        url = "../src/grabPokePage.php";
        var obj = {'pokeOffset': varible};    
    }

    /*
   var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function()
   {
    if (this.readyState == 4 && this.status == 200)
    {
     document.getElementById("demo").innerHTML = this.responseText;
    }
  };
  xhttp.open("GET", url, true);
  xhttp.send();
    */



    console.log("before post");
    $.post(url,obj,function(data){
         console.log(data);
         if(mode == 0)
         {
             console.log('in mode');
             loadPokemonData(data);
         }
        else if(mode == 1)
        {
            loadPokemonMenuData(data);      

        }

         
         console.log("inside post");
     });
    console.log("after post");
}

function loadPokemonMenuData(data)
{
    data = JSON.parse(data);
    console.log(data);
    var str = "";
    for(var i=0;i<data.length;i++)
    {
        str += "<span id='"+i+"' value='"+data[i].name+"' onClick='loadPokemon(this.attributes[1].nodeValue)'><input type='hidden' value='"+data[i].url+"'/>"+data[i].name+"</span><br/>";
      // console.log(str);    
    };

    console.log(str);
    $('#pokemonList').html(str);
}


function loadPokemonData(data)
{
    data = JSON.parse(data);
    console.log(data);
    console.log('in load pokemonData');
    console.log('data.image' + data.image);
    $('#poke').attr('src','..'+data.image);
    $('#pokemonName').html(data.name);
    $('#pokemonHeight').html(data.height);
    $('#pokemonWeight').html(data.weight);
    $('#pokemonSpecies').html(data.species.name);
    var ability = "";
    for(var i =0;i < data.abilities.length;i++)
        {
            ability = ability + data.abilities[i].ability.name + "<br/>";
        }
    $('#pokemonAbilities').html(ability);
}
      
</script>

</html>
